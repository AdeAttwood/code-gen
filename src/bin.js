#!/usr/bin/env node
'use strict';

const path = require('path');
const program = require('./cli');

// Init
process.dirname = path.resolve(__dirname);
process.templatePath = process.dirname + '/templates';

// Run cli
program.parse(process.argv);
program.args.length === 0 ? program.help() : '';
