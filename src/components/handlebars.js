const Handlebars = require('handlebars');

Handlebars.registerHelper('phpMethod', (config) => {
    let params = '';
    let docParams = '';
    let returns = config.returns ? config.returns : 'void';

    let length = config.params ? config.params.length : 0;
    for (let i = 0; i < length; i++) {
        params += '$' + config.params[i].name;
        docParams += `\n     * @param ${config.params[i].type} $${config.params[i].name}`;

        if (config.params[i].default) {
            params += ' = ' + config.params[i].default;
        }

        if ((i + 1) !== length) {
            params += ', ';
        } else {
            docParams += '\n     *';
        }
    }

    let method = `
    /**
     * Undocumented method
     *${docParams}
     * @return ${returns}
     */
    ${config.access} function ${config.name}(${params})
    {

    }
    `;
    return method;
});

module.exports = Handlebars;
