'use strict';

const fs = require('fs');
const path = require('path');
const childProcess = require('child_process');
const mkdir = require('mkdirp');

class Generator {

    constructor(config = {}) {
        this.Handlebars = require('../components/handlebars');

        this.templatePaths = [
            path.resolve(__dirname + '/../templates')
        ];

        this.templateName = config.templateName;
        this.name = config.name;
        this.args = config.args;
        this.basePath = config.basePath ? config.basePath : process.cwd();
        this.fileExtension = config.fileExtension;

        this.userName = childProcess.execSync('git config user.name').toString().trim();
        this.email    = childProcess.execSync('git config user.email').toString().trim();

        if (fs.existsSync(process.cwd() + '/composer.json')) {
            this.composerFile = require(process.cwd() + '/composer.json');
        }

        if (fs.existsSync(process.cwd() + '/package.json')) {
            this.packageFile = require(process.cwd() + '/package.json');
        }
    }

    buildArgs() {
        return Object.assign(this.args, {
            _filePath: this.fileName(),
            _fileExtension: this.fileExtension,
            _name: this.name,
            _userName: this.userName,
            _email: this.email,
            _composerFile: this.composerFile,
            _packageFile: this.packageFile
        });
    }

    toString() {
        let file = fs.readFileSync(this.getTemplate());
        let template = this.Handlebars.compile(file.toString(), {noEscape: true});
        return template(this.buildArgs());
    }

    fileName() {
        return `${this.basePath}/${this.name}.${this.fileExtension}`;
    }

    getTemplate() {
        for (let i = this.templatePaths.length - 1; i >= 0; i--) {
            const path = this.templatePaths[i];
            let _template = path + '/'  + this.templateName + '.hbs';
            if (fs.existsSync(_template)) {
                return _template;
            }
        }

        return false;
    }

    save() {
        mkdir.sync(this.basePath);
        fs.writeFileSync(this.fileName(), this.toString());
        return fs.existsSync(this.fileName());
    }

}

module.exports = Generator;
