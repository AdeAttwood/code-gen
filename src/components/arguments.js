'use strict';

const fs = require('fs');

class Arguments {

    /**
     * Validates that `value` is a valid path
     *
     * @param {String} value The path to validate
     *
     * @return {String|Boolean} The path if it exists of false.
     */
    isDir(value) {
        return fs.existsSync(value) ? value : false;
    }

    /**
     * Add the imputed value to the collection of values
     *
     * @param {String} value The imputed value
     * @param {Array} collection The current collection of values
     *
     * @returns {Array} The new collection with `value` added
     */
    collect(value, collection) {
        collection.push(value);
        return collection;
    }

}

module.exports = new Arguments();
