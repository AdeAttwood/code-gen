'use strict';

const fs      = require('fs');
const program = require('commander');

class Dotfile {

    constructor() {
        this.isLoaded = false;
        this.plugins = [];

        this.load();
    }

    load() {
        let jsPackage = process.env.HOME + '/.code-g';

        if (fs.existsSync(jsPackage)) {
            Object.assign(this, require(jsPackage));
            this.isLoaded = true;
        }
    }

    registerPlugins() {
        for (let i in this.plugins) {
            let plugin = this.plugins[i];
            plugin.register(program);
        }
    }

}

module.exports = new Dotfile();
