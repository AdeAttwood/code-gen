/* global console */

const chalk = require('chalk');
const log   = console.log;

module.exports = {

    /**
     * Renders text fomatted and colored for a success message
     *
     * @param {String} text The text to output
     *
     * @return {void}
     */
    success(text) {
        log(chalk.bgGreen.black(this.paddLines(text)));
    },

    /**
     * Renders text fomatted and colored for a error message
     *
     * @param {String} text The text to output
     *
     * @return {void}
     */
    error(text) {
        log(chalk.bgRed.white(this.paddLines('[ERROR] ' + text)));
    },

    /**
     * Renders an array into list
     *
     * @param {Array}  items The items to put into a list
     * @param {String} point The point for the list
     *
     * @return {void}
     */
    list(items, point = '- ') {
        log(this.paddLines(items.join('\n'), {
            padChar: '',
            padCharStart: point,
            padCharEnd: '',
            beforeRender: '',
            afterRender: ''
        }));
    },

    /**
     * Prints a new line char to the console
     *
     * @return {void}
     */
    newLine() {
        log();
    },

    /**
     * Pads lines out to the longest line
     * can also change items at the start.
     *
     * @param {String} text The text to pad
     * @param {Object} configuration The configuation object
     *
     * @return {String} The text with padding
     */
    paddLines(text, configuration = {}) {
        var config = Object.assign({
            padCharStart: ' ',
            padCharEnd: ' ',
            padChar: ' ',
        }, configuration);

        var out        = new String,
            lines      = text.split(/\r?\n/),
            outPutText = '',
            padLenth   = config.padCharEnd.concat(config.padCharStart).replace(/\r?\n/, '').length,
            length     = 0;

        lines.forEach((value, index) => {
            if (index == 0 && (value == ' ' || value == '')) {
                lines.splice(index, 1);
            }

            length = length < value.length + padLenth ? value.length + padLenth : length;
        });

        lines.forEach((value, index) => {
            var pad = new String(' ').repeat(length - value.length - padLenth);

            if (index == 0 && config.padChar != '') {
                outPutText += '\n';
            }

            outPutText += config.padCharStart + value + pad + config.padCharEnd;

            if (index + 1 != lines.length || config.padChar != '') {
                outPutText += '\n';
            }
        });

        out += config.padChar.repeat(length);
        out += outPutText;
        out += config.padChar.repeat(length);

        return out;
    }
};
