
'use strict';

const Generator = require('../components/generator');
const args = require('../components/arguments');
const output = require('../components/output');

module.exports = programme => {
    let command = programme.command('node:class');

    command.description('Generates a php file');
    command.option('-n, --name <value>', 'The name of the file');
    command.option('-p, --path <value>', 'The path to save the file', args.isDir);

    command.action(() => {
        if (command.path === false) {
            return output.error('-p, --path must be a valid dir');
        }

        let generator = new Generator({
            'templateName': 'node-class',
            'name': command.name.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase(),
            'basePath': command.path,
            'fileExtension': 'js',
            'args': {
                'class_name': command.name,
            }
        });

        generator.basePath = command.path;
        generator.save();
    });
};
