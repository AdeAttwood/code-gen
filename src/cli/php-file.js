'use strict';

const Generator = require('../components/generator');
const args = require('../components/arguments');
const output = require('../components/output');

module.exports = programme => {
    let command = programme.command('php:file');

    command.description('Generates a php file');
    command.option('-n, --name <value>', 'The name of the file');
    command.option('-s, --namespace <value>', 'The file namespace');
    command.option('-u, --uses [value]', 'The class to add as a use statement', args.collect, []);
    command.option('-p, --path <value>', 'The path to save the file', args.isDir);

    command.action(() => {
        if (command.path === false) {
            return output.error('-p, --path must be a valid dir');
        }

        let generator = new Generator({
            'templateName': 'php-file',
            'name': command.name,
            'basePath': command.path,
            'fileExtension': 'php',
            'args': {
                'class_name': command.name,
                'uses': command.uses,
                'namespace': command.namespace
            }
        });

        generator.basePath = command.path;
        generator.save();
    });
};
