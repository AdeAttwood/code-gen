'use strict';

const program     = require('commander');
const packageJson = require(__dirname + '/../../package.json');
const dotfile     = require('../components/dotfile');

// Create cli app
program.name(packageJson.name);
program.version(packageJson.version);
program.description(packageJson.description);

// Register commands
require('./php-class')(program);
require('./php-file')(program);

require('./node-class')(program);

require('./mocha-test.js')(program);

// Register plugins
dotfile.registerPlugins();

module.exports = program;
