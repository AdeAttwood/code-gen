'use strict';

const Generator = require('../components/generator');
const args = require('../components/arguments');
const output = require('../components/output');

module.exports = programme => {
    let command = programme.command('php:class');

    command.description('Generates a php class');
    command.option('-n, --name <value>', 'The name of the class');
    command.option('-e, --extends <value>', 'The full class name of the extension class');
    command.option('-s, --namespace <value>', 'The class namespace');
    command.option('-u, --uses [value]', 'The class to a as a use statement', args.collect, []);
    command.option('-p, --path <value>', 'The path to save the class', args.isDir, process.cwd());

    command.action(() => {
        if (command.path === false) {
            return output.error('-p, --path must be a valid dir');
        }

        let generator = new Generator({
            'templateName': 'php-class',
            'name': command.name,
            'basePath': command.path,
            'fileExtension': 'php',
            'args': {
                'class_name': command.name,
                'extends': command.extends,
                'uses': command.uses,
                'namespace': command.namespace
            }
        });

        generator.basePath = command.path;
        generator.save();
    });
};
