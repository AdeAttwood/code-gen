
'use strict';

const Generator = require('../components/generator');
const args = require('../components/arguments');
const output = require('../components/output');

module.exports = programme => {
    let command = programme.command('mocha:test');

    command.description('Generates a mocha js test file');
    command.option('-n, --name <value>', 'The name of the test');
    command.option('-p, --path <value>', 'The path to save the test', args.isDir, 'test');

    command.action(() => {
        if (command.path === false) {
            return output.error('-p, --path must be a valid dir');
        }

        let generator = new Generator({
            'templateName': 'mocha-test',
            'name': 'test-' + command.name.replace(/([a-z0-9])([A-Z])/g, '$1-$2').toLowerCase(),
            'basePath': command.path,
            'fileExtension': 'js',
            'args': {
                'test_name': command.name,
            }
        });

        generator.basePath = command.path;
        generator.save();
    });
};
