'use strict';

module.exports = {
    Generator: require('./components/generator'),
    Arguments: require('./components/arguments'),
    Output: require('./components/output')
};
