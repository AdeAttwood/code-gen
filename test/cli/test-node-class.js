/* global it, describe, beforeEach, afterEach */
'use strict';

const assert  = require('../assert');
const helpers = require('../helpers');

describe('NodeClass', function() {

    beforeEach(() => {
        helpers.createTmpDir();
    });

    afterEach(() => {
        helpers.removeTmpDir();
    });

    it('Should create a nodejs class', () => {
        helpers.runCommand('node:class', {
            name:'MyTestNodeClass',
            path: helpers.tmpPath()
        });

        assert.fileExists(helpers.tmpPath('my-test-node-class.js'));
    });

});

