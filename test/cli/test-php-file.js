/* global it, describe, beforeEach, afterEach */
'use strict';

const assert  = require('../assert');
const helpers = require('../helpers');

describe('php-file', function() {

    beforeEach(() => {
        helpers.createTmpDir();
    });

    afterEach(() => {
        helpers.removeTmpDir();
    });

    it('Should create a php file', () => {
        console.log(helpers.tmpPath());
        helpers.runCommand('php:file', {
            name:'the-php-file',
            path: helpers.tmpPath()
        });

        assert.fileExists(helpers.tmpPath('the-php-file.php'));
        assert.fileContains(helpers.tmpPath('the-php-file.php'), 'Undocumented file');
    });

});

