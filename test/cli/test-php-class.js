/* global it, describe, beforeEach, afterEach */
'use strict';

const assert  = require('../assert');
const helpers = require('../helpers');

describe('PhpClass', function() {

    beforeEach(() => {
        helpers.createTmpDir();
    });

    afterEach(() => {
        helpers.removeTmpDir();
    });

    it('Should create a php class', () => {
        helpers.runCommand('php:class', {
            name: 'MyTestClass',
            path: helpers.tmpPath()
        });

        assert.fileExists(helpers.tmpPath('MyTestClass.php'));
    });


});

