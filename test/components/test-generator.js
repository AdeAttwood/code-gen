/* global it, describe */
'use strict';

const assert    = require('assert');
const Generator = require('../../src/components/generator');

describe('Generator', () => {

    it('Should build the output file path', () => {
        let cwd = process.cwd();
        let generator = new Generator({
            'name': 'file-name',
            'basePath': cwd,
            'fileExtension': 'js'
        });

        assert.equal(generator.fileName(), cwd + '/file-name.js');
    });

    it('Should add template path', () => {
        let generator = new Generator();
        generator.templatePaths.push('new/template/path');
        assert.equal(generator.templatePaths.length, 2);
    });

    describe('#getTemplate()', () => {
        it('Should find the template', () => {
            let generator = new Generator({templateName: 'php-class'});
            assert.notEqual(generator.getTemplate(), false);
        });

        it('Should return false if template is not found', () => {
            let generator = new Generator({templateName: 'not-a-template'});
            assert.equal(generator.getTemplate(), false);
        });
    });

});
