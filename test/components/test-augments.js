/* global it, describe */
'use strict';

const assert = require('assert');
const args   = require('../../src/components/arguments');

describe('Augments', () => {

    describe('#isDir()', () => {
        it('Should return the path', () => {
            assert.equal(__dirname, args.isDir(__dirname));
        });

        it('Should return false if path dose not exist', () => {
            assert.equal(false, args.isDir(__dirname + '/not-a-dir'));
        });
    });

    describe('#collect()', () => {
        it('Should build an array of args', () => {
            let collection = [];
            args.collect('String 1', collection);
            args.collect('String 2', collection);
            assert.equal(2, collection.length);
        });
    });

});
