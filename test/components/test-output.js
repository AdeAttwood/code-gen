/* global it, describe */
'use strict';

const assert = require('assert');
const output = require('../../src/components/output');

describe('Output', () => {

    describe('#paddLines()', () => {
        it('Should return a string will padding text', () => {
            assert.equal('         \n Testing \n         ', output.paddLines('Testing'));
        });
    });

});
