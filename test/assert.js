'use strict';

const assert = require('assert');
const fs = require('fs');

module.exports = {

    equal(test1, test2) {
        return assert.equal(test1, test2);
    },

    notEqual(test1, test2) {
        return assert.notEqual(test1, test2);
    },

    fileExists(file) {
        return this.equal(true, fs.existsSync(file));
    },

    fileContains(file, string) {
        if (!fs.existsSync(file)) {
            assert.fail('File dose not exist');
            return false;
        }

        let contents = fs.readFileSync(file);
        return this.equal(true, contents.toString().indexOf(string) > -1);
    },


    fileDoseNotContains(file, string) {
        if (!fs.existsSync(file)) {
            assert.fail('File dose not exist');
            return false;
        }

        let contents = fs.readFileSync(file);
        return this.equal(true, contents.toString().indexOf(string) < 0);
    }

};
