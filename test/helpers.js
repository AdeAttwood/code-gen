'use strict';

const fs      = require('fs');
const command = require('commander').Command;

module.exports = {

    tmpPath(file = '') {
        let tmpPath = '/tmp/code-gen-tests';

        if (file !== '') {
            tmpPath += '/' + file;
        }

        return tmpPath;
    },

    createTmpDir() {
        if (!fs.existsSync(this.tmpPath())) {
            fs.mkdirSync(this.tmpPath());
        }
    },

    removeTmpDir() {
        fs.readdirSync(this.tmpPath()).forEach((file) => {
            fs.unlinkSync(this.tmpPath(file));
        });

        fs.rmdirSync(this.tmpPath());
    },

    cliApp() {
        let cmd = new command('Test Command');
        require('../src/cli/mocha-test')(cmd);
        require('../src/cli/node-class')(cmd);
        require('../src/cli/php-class')(cmd);
        require('../src/cli/php-file')(cmd);

        return cmd;
    },

    runCommand(name, _args = {}) {
        let app = this.cliApp();

        for (let i in app.commands) {
            if (app.commands[i]._name == name) {
                for (let j in _args) {
                    app.commands[i][j] = _args[j];
                }
            }
        }

        app.emit('command:' + name);
    }

};
