# Code Generator

Code generator is a tool built to created template file speeding up your
development. This package is the cli tool and at the core of the code gen
ecosystem. Code generator is build on a plugin biased system so you can build
your own generator and hopefully share it with the community.

## Installing

Code generator is built with nodejs and you can install it with `npm`. The
bellow command will install `code-g` globally if you only want to install it
locally, you can remove the `g` flag

~~~ sh
npm install -g code-g
~~~

## Usage

By default there is limited generators available, but its just a standard cli
tool. You can run the command `code-g -h` for more info.
